## Site Web NakedInTheDark
Website for the hyjal's WOW guilde NakedIntheDark.

## Motivation
This project is about training front dev with ReactJs

[![Build Status](https://travis-ci.org/akashnimare/foco.svg?branch=master)](https://travis-ci.org/akashnimare/foco)
[![Windows Build Status](https://ci.appveyor.com/api/projects/status/github/akashnimare/foco?branch=master&svg=true)](https://ci.appveyor.com/project/akashnimare/foco/branch/master)

## Code style
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)

## Tech/framework used
<b>Built with</b>
- [Symfony](https://symfony.com/)
- [ReactJs](https://reactjs.org/)

## Installation
Just Pull the project for now.

## Credits
Thx to Tibaka for this Website.

## License
MIT © [Tibaka|Randolph Thibault]()
