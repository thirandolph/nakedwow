import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";

import Duolc from "assets/img/faces/Duolc.jpg";
import Tibaka from "assets/img/faces/Tibaka2.jpg";
import Charonne from "assets/img/faces/Charonne1.jpg";
import Bara from "assets/img/faces/Bara.jpg";
import Dolga from "assets/img/faces/Dolga.jpg";
import Eze from "assets/img/faces/Eze1.jpg";
import Koyo from "assets/img/faces/Koyo.jpg";
import Fissu from "assets/img/faces/Fissu1.jpg";
import Dedra from "assets/img/faces/Dedra.jpg";
import Default from "assets/img/faces/Default.jpg";


import trombinoscopeStyle from "assets/jss/material-dashboard-react/views/trombinoscopeStyle.jsx";

const peoples = [
  {
    nom : "Duolc",
    pseudo : "Isariell",
    img1 : Duolc,
    img2 : "XXX",
    classe : "Paladin",
    spe : "Sacré",
    grade : "GM",
  },
  {
    nom : "Tibaka",
    pseudo : "Tibaka",
    img1 : Tibaka,
    img2 : "http://image.noelshack.com/fichiers/2018/38/4/1537459825-wtf.png",
    classe : "Voleur",
    spe : "Hors la loi",
    grade : "CO-GM",
  },
  {
    nom : "Charonne",
    pseudo : "Aethelred",
    img1 : Charonne,
    img2 : "http://image.noelshack.com/fichiers/2018/38/5/1537537166-img-20161217-101317.jpg",
    classe : "Paladin",
    spe : "Vindicte",
    grade : "Co-GM",
  },
  {
    nom : "Eze",
    pseudo : "Sõana",
    img1 : Eze,
    img2 : "http://image.noelshack.com/fichiers/2018/38/4/1537463944-gfhg.jpg",
    classe : "Prêtre",
    spe : "Discipline",
    grade : "Mascotte",
  },
  {
    nom : "Fissu",
    pseudo : "Pirea",
    img1 : Fissu,
    img2 : "http://image.noelshack.com/fichiers/2018/38/5/1537537268-cop.png",
    classe : "Druide",
    spe : "Equilibre",
    grade : "Capitaine",
  },
  {
    nom : "Koyomi",
    pseudo : "Koyomi",
    img1 : Koyo,
    img2 : "http://image.noelshack.com/fichiers/2018/38/5/1537556258-roule.jpg ",
    classe : "Chasseresse",
    spe : "Maîtrise des bêtes",
    grade : "Capitaine",
  },
  {
    nom : "Barahi",
    pseudo : "Kisenya",
    img1 : Bara,
    img2 : "XXX",
    classe : "Prêtre",
    spe : "Sacré",
    grade : "Quartier-maître",
  },
  {
    nom : "Wyriah",
    pseudo : "Wyriah",
    img1 : Default,
    img2 : "XXX",
    classe : "Paladin",
    spe : "Vindicte",
    grade : "Capitaine",
  },
  {
    nom : "Snipewolf",
    pseudo : "Dolga",
    img1 : Dolga,
    img2 : "XXX",
    classe : "Chevalier de la mort",
    spe : "Sang",
    grade : "Capitaine",
  },
  {
    nom : "Dedra",
    pseudo : "Nergul",
    img1 : Dedra,
    img2 : "http://image.noelshack.com/fichiers/2018/38/6/1537625749-39065074-1738709199539545-4959992550279610368-n.jpg",
    classe : "Chasseur de démons",
    spe : "Dévastation",
    grade : "Quartier-maître",
  },
];



class Trombinoscope extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  buildCards = (peoples, classes) => {
        const out = [];
          peoples.forEach(people => {
                  out.push(
                    <GridItem xs={12} sm={12} md={3}>
                      <Card profile>
                          <CardAvatar profile>
                            <a href="#pablo" onClick={e => e.preventDefault()}>
                              <img src={people.img1} alt="..."/>
                            </a>
                          </CardAvatar>
                          <CardBody profile>
                            <h6 className={classes.cardCategory}>{people.grade}</h6>
                            <h4 className={classes.cardTitle}>{people.nom}</h4>
                            <p className={classes.description}>{people.classe} | {people.spe}
                            </p>
                          </CardBody>
                        </Card>
                      </GridItem>);
          });
        return out;
}

  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          {this.buildCards(peoples, classes)}
        </GridContainer>
      </div>
    );
  }
}

Trombinoscope.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(trombinoscopeStyle)(Trombinoscope);
